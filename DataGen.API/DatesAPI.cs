﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGen.API
{
    public class DatesAPI
    {
        private DateTime _startDate;
        public DatesAPI()
        {

        }

        public DatesAPI(DateTime startDate)
        {
            _startDate = startDate;
        }


        /// <summary>
        /// returns a random date later than the date inject 
        /// into the API constructor
        /// </summary>
        /// <returns></returns>
        public DateTime GetRandomDate()
        {
            Random rand = new Random();
            int range = (DateTime.Today - _startDate).Days;
            return _startDate.AddDays(rand.Next(range));
        }



    }
}
