﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGen.API
{
    class SQLGenerator
    {
        private int _numRecords;

        public SQLGenerator() { }

        public SQLGenerator(int records)
        {
            _numRecords = records;
        }


        public string GenerateInsertStatements(List<string[]> data, string tablename)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string[] arr in data)
            {
                sb.Append("insert into " + tablename + "(");
                int recs = arr.Length;
                for (int i = 0; i < recs; i++)
                {

                    sb.Append(arr[i].ToString() + ", ");
                }
                sb.Append(");" + Environment.NewLine);
            }

            return sb.ToString();
        }
    }
}
