﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataGen.API
{
    public class NameAPI
    {
        private int _numRecords;

        public NameAPI()
        {

        }

        public NameAPI(int numrecords)
        {
            _numRecords = numrecords;
        }

        public List<string> GetFirstNames()
        {
            List<string> fnames = new List<string>();

            /// TODO:  Load the list of names into this list.
            List<string> names = new List<string>();
            if (!string.IsNullOrEmpty(_numRecords.ToString()))
            {
                fnames = (List<string>)names.Take(_numRecords);
            }

            return fnames;
        }

        public string GetRandomFirstName()
        {
            string name = string.Empty;
            List<string> listA = new List<string>();
            using (var reader = new StreamReader(@"../DataGen.API\Data\fnames.csv"))
            {
                int count = 0;
                while (!reader.EndOfStream)
                {
                  
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    listA.Add(values[0]);
                }

            }
            var random = new Random();
            int index = random.Next(listA.Count);
            return listA.ToArray()[index];
        }

        public string GetRandmomSurName()
        {
            string name = string.Empty;
            List<string> listA = new List<string>();
            using (var reader = new StreamReader(@"../DataGen.API\Data\Surnames.csv"))
            {
                int count = 0;
                while (!reader.EndOfStream)
                {

                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    listA.Add(values[0]);
                }

            }
            var random = new Random();
            int index = random.Next(listA.Count);
            return listA.ToArray()[index];
        }

        private int GetRandomNum(int end)
        {
            Random r = new Random();
            int rInt = r.Next(0, end);
            return rInt;
        }

        /// <summary>
        /// Returns an array containing a random first name and a random last name.
        /// </summary>
        /// <returns></returns>
        public string[] GetFullName()
        {
            string[] fullname = { GetRandomFirstName(), GetRandmomSurName() };
            return fullname;
        }
    }
}

