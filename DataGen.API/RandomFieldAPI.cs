﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGen.API
{
    public class RandomFieldAPI
    {
        public RandomFieldAPI()
        {

        }


        public int RandomNumberField(int digits)
        {
            Random r = new Random();
            string num = string.Empty;
            for (int i = 0; i < digits; i++)
            {
                num += r.Next(0, digits).ToString();
            }
            return Int32.Parse(num);
        }
    }
}
