#pragma checksum "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "281057d3782166a0f48c5205faeaff96292593aa"
// <auto-generated/>
#pragma warning disable 1591
namespace DataGenerator.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using DataGenerator;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using DataGenerator.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\kevin\source\repos\datagen\DataGenerator\_Imports.razor"
using Blazorise;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
using DataGen.API;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
using DataGenerator.UI;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/generator")]
    public partial class Generator : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "form-group");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "row");
            __builder.AddAttribute(4, "style", "width: 20%;");
            __builder.AddMarkupContent(5, "<label>Field Name</label>\r\n        ");
            __builder.OpenElement(6, "input");
            __builder.AddAttribute(7, "type", "text");
            __builder.AddAttribute(8, "class", "form-control");
            __builder.AddAttribute(9, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 7 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
                                   fieldName

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(10, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => fieldName = __value, fieldName));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n        ");
            __builder.AddMarkupContent(12, "<label>Number of rcords</label>\r\n        ");
            __builder.OpenElement(13, "input");
            __builder.AddAttribute(14, "type", "text");
            __builder.AddAttribute(15, "class", "form-control");
            __builder.AddAttribute(16, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 9 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
                                   numRecords

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(17, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => numRecords = __value, numRecords));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\r\n        ");
            __builder.OpenComponent<Blazorise.Label>(19);
            __builder.AddAttribute(20, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.AddContent(21, "TableName");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(22, "\r\n        ");
            __builder.OpenElement(23, "input");
            __builder.AddAttribute(24, "type", "text");
            __builder.AddAttribute(25, "class", "form-control");
            __builder.AddAttribute(26, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 11 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
                                   tableName

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(27, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => tableName = __value, tableName));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n");
            __builder.OpenComponent<DataGenerator.UI.DataGenUI>(29);
            __builder.AddAttribute(30, "fieldName", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 14 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
                                        fieldName

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(31, "TableName", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 14 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
                                                               tableName

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(32, "numRecords", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 14 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
                                                                                       numRecords

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 19 "C:\Users\kevin\source\repos\datagen\DataGenerator\Pages\Generator.razor"
       
    string fieldName;
    int numRecords;
   string tableName;

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
